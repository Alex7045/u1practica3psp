Link a gitlab: https://gitlab.com/Alex7045/u1practica3psp/
Ejercicio 4: ejecutar el jar encontrado en out/artifacts/Ejercicio4_jar/Ejercicio4.jar 
Posibles argumentos:
-(Nada), nos pedira palabras en ingles para traducir si encuentra alguna del programa la devolverá traducida a Español el programa finaliza si decimos "stop".
- (-f ficheroParaLeer.txt) Nos traducira el fichero con palabras en Inglés (1 palabra por linea) y lo traducirá si encuentra las palabras en el código a Español.
- (-d diccionario.txt) Nos leerá el diccionario (palabraInges->>PalabraEspañol) y nos preguntará por palabras, si encuentra la palabra del diccionario pasado como argumento la traducirá a Español, si ponemos "stop" el programa finaliza.
- (ambos argumentos -d diccionario -f fichero (no importa el orden)) Nos leerá ambos ficheros y traducira el texto del fichero -f con las palabras que encuentre en el diccionario.

Ejercicio5: ejecutar el jar encontrado en out/artifacts/Ejercicio5_jar/Ejercicio5.jar
Posibles argumentos:
- Pasar tantos ficheros como se desee, estos ficheros deberán contener numeros (1 por linea) y el programa sumara todos los numeros encontrados en los ficheros y lo devolverá por pantalla, también se guarda el valor del fichero en uno nuevo llamado Totals.txt encontrado en la carpeta donde esta el .jar. 
