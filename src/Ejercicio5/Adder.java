package Ejercicio5;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Adder {
    public static void main(String[] args) {
        try {
            //se lee el fichero mandado como argumentos
            BufferedReader br = new BufferedReader(new FileReader(args[0]));
            String linea = "";
            int numeros = 0;
            //se recorren las lineas del fichero y suma todos los valores
            while ((linea = br.readLine()) != null) {
                numeros += Integer.parseInt(linea);
            }
            System.out.println(numeros);
            br.close();
        } catch (IOException e) {
            //si ejecutamos el jar sin fichero nos mandara este mensaje
            System.err.println("No existe el archivo");
        }
    }
}
