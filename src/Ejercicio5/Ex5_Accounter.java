package Ejercicio5;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Ex5_Accounter {
    public static void main(String[] args) {

        List<String> list = Arrays.asList(args);
        String command = "java -jar out/artifacts/Adder_jar/Adder.jar";
        ProcessBuilder pb;
        Scanner numeros;
        String salida;
        File file = new File("Totals.txt");

        int total = 0;

        List<String> argsList = new ArrayList<>(Arrays.asList(command.split(" ")));
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            for (String s : list) {
                argsList.add(s);
                //iniciamos un nuevo proceso Adder.jar con cada uno de los
                //archivos que mandemos como argumento de una lista
                pb = new ProcessBuilder(argsList);
                Process process = pb.start();
                //cogo los numeros de proceso hijo y los voy sumando
                numeros = new Scanner(process.getInputStream());
                salida = numeros.nextLine();
                total += Integer.parseInt(salida);
                //borro el ultimo archivo para que se coloque el siguiente
                //en la posicion 0 y vuelva a crear otro proceso
                argsList.remove(s);
            }
            //el bucle finalizara cuando no hayan mas archivos en la lista
            //finalmente guardo el totoal en un archivo llamado Totals.txt y muestro por pantalla el total
            System.out.println("Total: " + total);
            bw.write("Total -> " + total + "\n");
            bw.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}


