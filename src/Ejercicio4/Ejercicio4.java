package Ejercicio4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Ejercicio4 {
    private static String[] ARGS;

    public static void main(String[] args) throws IOException {
        String sisOp = System.getProperty("os.name").toLowerCase();
        String comando = "";
        ARGS = args;

        //Se comprueba el sistema operativo para marcar la ruta
        if (sisOp.equals("windows 10")) {
            comando = "java -jar out\\artifacts\\Translator_jar\\Translator.jar";
        } else {
            comando = "java -jar out/artifacts/Translator_jar/Translator.jar";
        }


        List<String> argsList = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);
        BufferedWriter bw = null;
        try {
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            bw = new BufferedWriter(osw);

            Scanner procesoSC = new Scanner(process.getInputStream());


            Scanner sc = new Scanner(System.in);
            String linea = "";
            //compruebo si mando un -f en los argumentos para poder mandar lineas de texto
            //por medio del Scanner
            if (!tieneF(ARGS)) {
                linea = sc.nextLine();
                while (!linea.equals("stop")) {
                    bw.write(linea);
                    bw.newLine();
                    bw.flush();
                    System.out.println(procesoSC.nextLine());
                    linea = sc.nextLine();
                }
                //Si no mando directamente el fichero que vaya despues del -f
            } else {
                ArrayList<String> texto = leerArchivo();
                for (int i = 0; i < texto.toArray().length; i++) {
                    bw.write(texto.get(i));
                    bw.newLine();
                    bw.flush();
                    System.out.println(procesoSC.nextLine());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Ejercicio4.class.getName());
            ex.printStackTrace();
        }
    }

    private static boolean tieneF(String[] args) {
        for (int i = 0; i < ARGS.length; i++) {
            if (ARGS[i].equals("-f")) {
                return true;
            }
        }
        return false;
    }


    private static ArrayList<String> leerArchivo() throws IOException {
        int posicion = 0;
        if (ARGS[0].equals("-f")) {
            posicion = 1;
        } else {
            posicion = 3;
        }
        BufferedReader br = new BufferedReader(new FileReader(ARGS[posicion]));
        String linea = br.readLine();
        ArrayList<String> texto = new ArrayList<>();

        int i = 0;
        while (linea != null) {
            linea = br.readLine();
            texto.add(linea);
            i++;
        }
        br.close();
        return texto;
    }
}


