package Ejercicio4;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Translator {
    private static HashMap<String, String> hashMap;
    private static String[] ARGS;

    public static void main(String[] args) throws IOException {
        hashMap = new HashMap<>();
        Scanner leer = new Scanner(System.in);
        ARGS = args;
        //si no hay argumentos, inicializa el hash al del programa con x palabras
        //ya creadas y va preguntando por cadenas de texto si encuentra el hash
        //una palabra en su posesion envia la traduccion parara cuando escribamos stop
        if (ARGS.length == 0) {
            inicializarHash();
            String palabra = leer.nextLine();
            while (!palabra.equals("stop")) {
                if (hashMap.containsKey(palabra)) {
                    System.out.println(hashMap.get(palabra));
                } else {
                    System.out.println("desconocido");
                }
                palabra = leer.nextLine();
            }
            //si mandamos un -d y un diccionario se ejecutara esta condicion
            //la cual inicializa en hash con un diccionario.txt que mandemos
            //como argumento, funciona igual que la condicion anterior pero
            //las palabras del hash son del diccionario
        } else if (tieneD(ARGS) && !tieneF(ARGS)) {
            inicializarHashDiccionario();
            String palabra = leer.nextLine();
            try {
                while (!palabra.equals("stop")) {
                    if (hashMap.containsKey(palabra)) {
                        System.out.println(hashMap.get(palabra));
                    } else {
                        System.out.println("desconocido");
                    }
                    palabra = leer.nextLine();
                }
            } catch (Exception e) {
                System.out.println("Error! Diccionario no encontrado");
            }
            //en esta condicion si pasamos un "-f fichero.txt" nos traducira
            //el fichero que mandemos con el hash por defecto del codigo
        } else if (tieneF(ARGS) && !tieneD(ARGS)) {
            inicializarHash();
            ArrayList <String> texto = leerArchivo();
            String traduccion = "";
            for (int i = 0; i < texto.size(); i++){
                if (hashMap.containsKey(texto.get(i))) {
                    traduccion = hashMap.get(texto.get(i));
                    System.out.println(traduccion);
                } else {
                    System.out.println("desconocido");
                }
            }
            //finalmente si pasamos diccionario y fichero se traducira el fichero
            //con las palabras encontradas dentro del diccionario
        } else {
            inicializarHashDiccionario();
            ArrayList <String> texto = leerArchivo();
            String traduccion = "";
            for (int i = 0; i < texto.size(); i++){
                if (hashMap.containsKey(texto.get(i).toString())) {
                    traduccion = hashMap.get(texto.get(i));
                    System.out.println(traduccion);
                } else {
                    System.out.println("desconocido");
                }
            }
        }
    }


    private static void inicializarHash() {
        hashMap.put("hello", "hola");
        hashMap.put("dirt", "tierra");
        hashMap.put("car", "coche");
        hashMap.put("cat", "gato");
        hashMap.put("poop", "caca");
    }

    //metodo que recorre los argumentos y si encuentra -d devuelve true
    private static boolean tieneD(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-d")) {
                return true;
            }
        }
        return false;
    }

    //lee el txt que contiene el diccionario con la siguiente serialización
    //palabraIngles->>palabraEspañol y inicializa el hash
    private static void inicializarHashDiccionario() throws IOException {
        int posicion = 0;
        if (ARGS[0].equals("-d")) {
            posicion = 1;
        } else {
            posicion = 3;
        }
        BufferedReader br = new BufferedReader(new FileReader(ARGS[posicion]));
        String linea = br.readLine();
        while (linea != null) {
            String[] lineasArchivo = linea.split("->>");
            hashMap.put(lineasArchivo[0], lineasArchivo[1]);
            linea = br.readLine();
        }
        br.close();
    }

    //metodo que recorre args y si encuentra un -f devuelve true
    private static boolean tieneF(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-f")) {
                return true;
            }
        }
        return false;
    }

    //metodo que lee el archivo y devuelve como array list las palabras
    //en distintas posiciones
    private static ArrayList<String> leerArchivo() throws IOException {
        int posicion = 0;
        if (ARGS[0].equals("-f")) {
            posicion = 1;
        } else {
            posicion = 3;
        }
        BufferedReader br = new BufferedReader(new FileReader(ARGS[posicion]));
        String linea = "";
        ArrayList <String> texto = new ArrayList<>();
        int i = 0;
        while ((linea = br.readLine()) != null) {
            texto.add(linea);
        }
        br.close();
        return texto;
    }
}
